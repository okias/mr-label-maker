#!/usr/bin/env python3

# Copyright © 2020-2022 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import os
import sys
import time

from . import project
from . import mesa

def main():
    parser = argparse.ArgumentParser(description='Label issues and merge requests on GitLab')
    parser.add_argument('--dry-run', '-d',
                        dest='dry_run',
                        action='store_const',
                        const=True,
                        default=False,
                        help="don't apply any changes")
    parser.add_argument('--project', '-p',
                        dest='project',
                        required=True,
                        choices=['mesa'],
                        help='set project')
    parser.add_argument('--token', '-t',
                        dest='token',
                        help="set GitLab API token")
    parser.add_argument('--issues', '-i',
                        dest='issues',
                        nargs='?',
                        default=0,
                        const=-1,
                        type=int,
                        help='process issues')
    parser.add_argument('--merge-requests', '-m', '--mrs',
                        dest='mrs',
                        nargs='?',
                        default=0,
                        const=-1,
                        type=int,
                        help='process merge requests')
    parser.add_argument('--label', '-l',
                        dest='label',
                        nargs=1,
                        default='None',
                        help="search for issues/MRs with this label (default=None)")
    parser.add_argument('--state', '-s',
                        dest='state',
                        nargs=1,
                        default='opened',
                        choices=['opened', 'closed', 'merged', 'all'],
                        help="search for issues/MRs in this state (default=opened)")
    parser.add_argument('--ignore-label-history',
                        dest='ignore_label_history',
                        action='store_const',
                        const=True,
                        default=False,
                        help="process issues with a history of label changes (default=false)")
    parser.add_argument('--poll',
                        dest='poll',
                        default=0,
                        type=int,
                        help='poll GitLab every POLL seconds')

    args = parser.parse_args()

    if args.project == 'mesa':
        proj = mesa.Mesa()
    else:
        print('unhandled project name: ' + args.project)
        sys.exit(project.BUG)

    token = None
    if args.token:
        token = args.token

    if token is None:
        token = os.environ.get('GITLAB_TOKEN')

    if token is None:
        print('GitLab API token not set. Use either GITLAB_TOKEN environment variable or --token option.')
        sys.exit(project.USER_ERROR)

    proj.set_token(token)
    proj.set_dry_run(args.dry_run)
    proj.set_label(args.label)
    proj.set_state(args.state)
    proj.set_ignore_label_history(args.ignore_label_history)

    proj.connect()

    while True:
        if args.issues:
            proj.process_issues(args.issues)

            if args.mrs:
                print('\n\n')

        if args.mrs:
            proj.process_mrs(args.mrs)

        if args.poll == 0:
            break
        time.sleep(args.poll)

main()
